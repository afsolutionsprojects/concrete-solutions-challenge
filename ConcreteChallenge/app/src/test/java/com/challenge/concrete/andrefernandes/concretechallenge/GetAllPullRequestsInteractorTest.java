package com.challenge.concrete.andrefernandes.concretechallenge;

import com.challenge.concrete.andrefernandes.concretechallenge.domain.executor.Executor;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.executor.MainThread;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.interactors.GetPullRequestInteractor;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.interactors.impl.GetPullRequestImpl;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.pullrequests.PullRequestsModel;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.repository.PullRequestRepository;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.repository.impl.PullRequestRepositoryImpl;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.presenters.PullRequestPresenter;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.presenters.impl.PullRequestPresenterImpl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by andrefernandes on 9/19/16.
 */
public class GetAllPullRequestsInteractorTest {

    GetPullRequestImpl interactor;

    private final String LOGIN = "fake_login";
    private final String REPO = "fake_repo";

    @Mock
    private Executor executor;
    @Mock private MainThread mainThread;
    @Mock private PullRequestPresenterImpl mockedCallback;
    @Mock private PullRequestRepository repository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        interactor = new GetPullRequestImpl(
                executor,
                mainThread,
                mockedCallback,
                repository,
                LOGIN,
                REPO
        );
    }

    @Test
    public void testGetAllRepositories() throws Exception{

        List<PullRequestsModel> fakePullRequestListModel = Mockito.mock(List.class);

        when(repository.getPullRequest(LOGIN, REPO)).thenReturn(fakePullRequestListModel);

        interactor.run();
        verify(repository).getPullRequest(LOGIN, REPO);
        Mockito.verifyNoMoreInteractions(repository);
    }


}
