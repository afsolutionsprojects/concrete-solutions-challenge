package com.challenge.concrete.andrefernandes.concretechallenge;

import com.challenge.concrete.andrefernandes.concretechallenge.domain.executor.Executor;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.executor.MainThread;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.interactors.GetAllRepositoriesInteractor;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.interactors.impl.GetAllRepositoriesImpl;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.repositories.RepositoryModel;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.repository.RepositoriesRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by andrefernandes on 9/19/16.
 */
public class GetAllRepositoriesInteractorTest {

    GetAllRepositoriesImpl interactor;

    private static final int PAGE = 1;

    @Mock private Executor executor;
    @Mock private MainThread mainThread;
    @Mock private GetAllRepositoriesImpl.Callback mockedCallback;
    @Mock private RepositoriesRepository repository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        interactor = new GetAllRepositoriesImpl(
                executor,
                mainThread,
                mockedCallback,
                repository,
                PAGE
        );
    }

    @Test
    public void testGetAllRepositories() throws Exception{

        RepositoryModel fakeRepositoryModel = Mockito.mock(RepositoryModel.class);

        when(repository.getAllRepositories(PAGE)).thenReturn(fakeRepositoryModel);
        interactor.run();
        verify(repository).getAllRepositories(PAGE);
        Mockito.verifyNoMoreInteractions(repository);
    }

}
