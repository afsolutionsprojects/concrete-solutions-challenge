package com.challenge.concrete.andrefernandes.concretechallenge.domain.interactors.base;

/**
 * Created by andrefernandes on 9/13/16.
 */
public interface Interactor {

    void execute();
}
