package com.challenge.concrete.andrefernandes.concretechallenge.domain.network.callbacks;

import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by andrefernandes on 9/13/16.
 */
public abstract class HandlerCallback<T> implements Callback<T> {

    @Override
    public void onFailure(Call<T> call, Throwable t) {

    }


}
