package com.challenge.concrete.andrefernandes.concretechallenge;

import android.app.Application;

import net.danlew.android.joda.JodaTimeAndroid;

/**
 * Created by andrefernandes on 9/17/16.
 */
public class ChallengeConcreteApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);
    }

}
