package com.challenge.concrete.andrefernandes.concretechallenge.presentation.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.challenge.concrete.andrefernandes.concretechallenge.R;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.executor.Executor;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.executor.MainThread;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.executor.impl.ThreadExecutor;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.pullrequests.PullRequestsModel;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.repositories.RepositoryItem;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.repository.impl.PullRequestRepositoryImpl;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.presenters.PullRequestPresenter;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.presenters.impl.PullRequestPresenterImpl;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.ui.adapters.PullRequestAdapter;
import com.challenge.concrete.andrefernandes.concretechallenge.threading.MainThreadImpl;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PullRequestActivity extends AppCompatActivity implements PullRequestPresenter.View {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.opened_text)
    TextView openedText;
    @Bind(R.id.closed_text)
    TextView closedText;
    @Bind(R.id.pullrequest_list)
    RecyclerView pullRequestList;
    @Bind(R.id.text_generic_error)
    TextView textGenericError;
    @Bind(R.id.btn_try_again)
    Button btnTryAgain;
    @Bind(R.id.error_container)
    LinearLayout errorContainer;
    @Bind(R.id.container_content_pull_request)
    RelativeLayout containerContentPullRequest;
    @Bind(R.id.progress)
    ProgressBar progress;

    private String repositoryName;
    private String ownerLogin;

    private PullRequestPresenter presenter;
    private PullRequestAdapter adapter;

    private static final String OWNER_LOGIN = "OWNER_LOGIN";
    private static final String REPOSITORY_NAME = "REPOSITORY_NAME";

    public static Intent getCallingIntent(Activity activity, RepositoryItem repositoryItem) {
        Intent intent = new Intent(activity, PullRequestActivity.class);
        intent.putExtra(OWNER_LOGIN, repositoryItem.getOwnerLogin());
        intent.putExtra(REPOSITORY_NAME, repositoryItem.getName());

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);
        ButterKnife.bind(this);
        unbundle();
        setupToolbar();
        setupPresenter();
        setupRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();

        presenter.resume();
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(repositoryName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setupPresenter() {
        Executor executor = new ThreadExecutor();
        MainThread thread = new MainThreadImpl();
        presenter = new PullRequestPresenterImpl(executor, thread, this, new PullRequestRepositoryImpl(),
                ownerLogin, repositoryName);
    }

    private void setupRecyclerView() {
        adapter = new PullRequestAdapter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        pullRequestList.setLayoutManager(linearLayoutManager);
        pullRequestList.setAdapter(adapter);
    }

    private void unbundle() {
        repositoryName = getIntent().getStringExtra(REPOSITORY_NAME);
        ownerLogin = getIntent().getStringExtra(OWNER_LOGIN);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (adapter != null) {
            pullRequestList.setAdapter(adapter);
        }
    }

    @Override
    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        containerContentPullRequest.setVisibility(View.GONE);
        errorContainer.setVisibility(View.VISIBLE);
        textGenericError.setText(getString(R.string.error_generic_text));
    }

    @Override
    public void showPullRequests(List<PullRequestsModel> pullRequestsModel) {
        errorContainer.setVisibility(View.GONE);
        adapter.addNewPullRequest(pullRequestsModel);
        containerContentPullRequest.setVisibility(View.VISIBLE);
    }

    @Override
    public void insertOpenedAndClosed(String opened, String closed) {
        openedText.setText(getString(R.string.opened_pull_requests_text, opened));
        closedText.setText(getString(R.string.closed_pull_requests_text, closed));
    }

    @OnClick(R.id.btn_try_again)
    public void onClick() {
        presenter.resume();
    }
}
