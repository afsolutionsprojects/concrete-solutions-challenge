package com.challenge.concrete.andrefernandes.concretechallenge.domain.interactors;

import com.challenge.concrete.andrefernandes.concretechallenge.domain.interactors.base.Interactor;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.repositories.RepositoryModel;

import java.util.List;

/**
 * Created by andrefernandes on 9/13/16.
 */
public interface GetAllRepositoriesInteractor extends Interactor {

    interface Callback {
        void onRepositoriesRetrieved(RepositoryModel repositories);
        void onRetrieveFailed();
    }

    void loadRepositoriesByPage(int page);
}
