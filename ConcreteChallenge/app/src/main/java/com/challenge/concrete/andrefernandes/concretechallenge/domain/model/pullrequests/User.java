package com.challenge.concrete.andrefernandes.concretechallenge.domain.model.pullrequests;

/**
 * Created by andrefernandes on 9/18/16.
 */
public class User {

    private String avatar_url;

    private String gravatar_id;

    private String id;

    private String login;

    public String getAvatar_url ()
    {
        return avatar_url;
    }

    public void setAvatar_url (String avatar_url)
    {
        this.avatar_url = avatar_url;
    }

    public String getGravatar_id ()
    {
        return gravatar_id;
    }

    public void setGravatar_id (String gravatar_id)
    {
        this.gravatar_id = gravatar_id;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getLogin ()
    {
        return login;
    }

    public void setLogin (String login)
    {
        this.login = login;
    }

}
