package com.challenge.concrete.andrefernandes.concretechallenge.domain.interactors;

import com.challenge.concrete.andrefernandes.concretechallenge.domain.interactors.base.Interactor;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.pullrequests.PullRequestsModel;

import java.util.List;

/**
 * Created by andrefernandes on 9/18/16.
 */
public interface GetPullRequestInteractor extends Interactor {

    interface Callback {
        void onPullRequestRetrieved(List<PullRequestsModel> pullRequests);
        void onRetrieveFailed();
    }
}
