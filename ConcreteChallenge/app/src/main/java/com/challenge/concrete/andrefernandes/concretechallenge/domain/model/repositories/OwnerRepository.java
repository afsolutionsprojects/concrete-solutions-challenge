package com.challenge.concrete.andrefernandes.concretechallenge.domain.model.repositories;

/**
 * Created by andrefernandes on 9/17/16.
 */
public class OwnerRepository {

    private String avatar_url;

    private String id;

    private String login;

    public String getAvatar_url ()
    {
        return avatar_url;
    }

    public void setAvatar_url (String avatar_url)
    {
        this.avatar_url = avatar_url;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getLogin ()
    {
        return login;
    }

    public void setLogin (String login)
    {
        this.login = login;
    }
}
