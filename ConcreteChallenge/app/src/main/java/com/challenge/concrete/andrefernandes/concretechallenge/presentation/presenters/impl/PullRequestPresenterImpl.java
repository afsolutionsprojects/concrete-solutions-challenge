package com.challenge.concrete.andrefernandes.concretechallenge.presentation.presenters.impl;

import com.challenge.concrete.andrefernandes.concretechallenge.domain.executor.Executor;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.executor.MainThread;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.interactors.GetPullRequestInteractor;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.interactors.impl.GetPullRequestImpl;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.pullrequests.PullRequestsModel;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.repository.PullRequestRepository;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.presenters.PullRequestPresenter;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.presenters.base.AbstractPresenter;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.ui.utils.DateUtils;

import java.util.List;

/**
 * Created by andrefernandes on 9/18/16.
 */
public class PullRequestPresenterImpl extends AbstractPresenter implements PullRequestPresenter, GetPullRequestInteractor.Callback {

    private PullRequestPresenter.View view;
    private PullRequestRepository repository;
    private String login;
    private String repositoryName;

    private final String OPEN_TEXT = "open";


    public PullRequestPresenterImpl(Executor executor, MainThread mainThread, PullRequestPresenter.View view,
                                    PullRequestRepository repository, String login, String repositoryName) {
        super(executor, mainThread);
        this.view = view;
        this.repository = repository;
        this.login = login;
        this.repositoryName = repositoryName;
    }

    @Override
    public void resume() {
        view.showProgress();
        GetPullRequestInteractor interactor = new GetPullRequestImpl(executor, mainThread, this, repository, login, repositoryName);
        interactor.execute();
    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onPullRequestRetrieved(List<PullRequestsModel> pullRequests) {
        view.hideProgress();
        int opened = calculateOpenedPullRequests(pullRequests);
        int closed = calculateClosedPullRequests(opened, pullRequests);
        view.insertOpenedAndClosed(String.valueOf(opened), String.valueOf(closed));
        view.showPullRequests(pullRequests);
    }

    private int calculateClosedPullRequests(int opened, List<PullRequestsModel> pullRequests) {

        return pullRequests.size() - opened;
    }

    private int calculateOpenedPullRequests(List<PullRequestsModel> pullRequests) {

        int count = 0;

        for (PullRequestsModel pullRequestsModel : pullRequests) {
            if (pullRequestsModel.getState().equals(OPEN_TEXT)) {
                count++;
            }
        }

        return count;

    }

    @Override
    public void onRetrieveFailed() {
        view.showError("");
    }
}
