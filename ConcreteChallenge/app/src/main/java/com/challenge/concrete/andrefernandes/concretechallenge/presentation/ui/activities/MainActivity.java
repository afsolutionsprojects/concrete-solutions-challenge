package com.challenge.concrete.andrefernandes.concretechallenge.presentation.ui.activities;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.challenge.concrete.andrefernandes.concretechallenge.R;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.executor.Executor;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.executor.MainThread;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.executor.impl.ThreadExecutor;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.repositories.RepositoryModel;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.repository.impl.RepositoriesRepositoryImpl;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.presenters.MainPresenter;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.presenters.impl.MainPresenterImpl;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.ui.adapters.RepositoriesAdapter;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.ui.listeners.EndlessRecyclerViewListener;
import com.challenge.concrete.andrefernandes.concretechallenge.threading.MainThreadImpl;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by andrefernandes on 9/14/16.
 */
public class MainActivity extends AppCompatActivity implements MainPresenter.View {

    MainPresenter presenter;
    RepositoriesAdapter adapter;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.repositories_list)
    RecyclerView recyclerView;
    @Bind(R.id.text_generic_error)
    TextView textGenericError;
    @Bind(R.id.btn_try_again)
    Button btnTryAgain;
    @Bind(R.id.error_container)
    LinearLayout errorContainer;
    @Bind(R.id.container_content_main)
    RelativeLayout containerContentMain;
    @Bind(R.id.cl_main)
    CoordinatorLayout clMain;
    @Bind(R.id.progress)
    ProgressBar progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupPresenter();
        setupToolbar();
        setupRecyclerView();
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
    }

    private void setupRecyclerView() {
        adapter = new RepositoriesAdapter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(new EndlessRecyclerViewListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                presenter.loadMoreRepositores(page);
            }
        });

        recyclerView.setAdapter(adapter);
    }


    private void setupPresenter() {
        Executor executor = new ThreadExecutor();
        MainThread thread = new MainThreadImpl();
        presenter = new MainPresenterImpl(executor, thread, this, new RepositoriesRepositoryImpl());
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.resume();
    }

    @Override
    public void showRepositories(RepositoryModel repositoryModelList) {
        errorContainer.setVisibility(View.GONE);
        adapter.addNewRepositories(repositoryModelList);
        containerContentMain.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSnackbarError() {
        Snackbar.make(clMain, getString(R.string.error_cannot_load_more), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(View.GONE);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (adapter != null) {
            recyclerView.setAdapter(adapter);
        }
    }

    @Override
    public void showError(String message) {
        containerContentMain.setVisibility(View.GONE);
        errorContainer.setVisibility(View.VISIBLE);
        textGenericError.setText(getString(R.string.error_generic_text));

    }

    @OnClick({R.id.text_generic_error, R.id.btn_try_again})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_try_again:
                presenter.resume();
                break;
        }
    }
}
