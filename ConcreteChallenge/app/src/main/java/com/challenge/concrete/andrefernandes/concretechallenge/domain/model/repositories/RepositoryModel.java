package com.challenge.concrete.andrefernandes.concretechallenge.domain.model.repositories;

import java.util.List;

/**
 * Created by andrefernandes on 9/13/16.
 */
public class RepositoryModel {

    private String incomplete_results;

    private List<RepositoryItem> items;

    private String total_count;

    public String getIncomplete_results ()
    {
        return incomplete_results;
    }

    public void setIncomplete_results (String incomplete_results)
    {
        this.incomplete_results = incomplete_results;
    }

    public List<RepositoryItem> getItems ()
    {
        return items;
    }

    public void setItems (List<RepositoryItem> items)
    {
        this.items = items;
    }

    public String getTotal_count ()
    {
        return total_count;
    }

    public void setTotal_count (String total_count)
    {
        this.total_count = total_count;
    }

    public RepositoryItem getItemByPosition(int position){
        return items.get(position);
    }

}
