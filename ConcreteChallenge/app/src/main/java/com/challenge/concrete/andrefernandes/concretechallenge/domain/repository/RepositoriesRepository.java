package com.challenge.concrete.andrefernandes.concretechallenge.domain.repository;

import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.repositories.RepositoryModel;

import java.util.List;

/**
 * Created by andrefernandes on 9/13/16.
 */
public interface RepositoriesRepository{

    RepositoryModel getAllRepositories(int page);
}
