package com.challenge.concrete.andrefernandes.concretechallenge.domain.model.repositories;

/**
 * Created by andrefernandes on 9/17/16.
 */
public class RepositoryItem {

    private String description;

    private OwnerRepository owner;

    private String language;

    private String forks_count;

    private String name;

    private String stargazers_count;

    private String full_name;

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public OwnerRepository getOwner ()
    {
        return owner;
    }

    public void setOwner (OwnerRepository owner)
    {
        this.owner = owner;
    }

    public String getLanguage ()
    {
        return language;
    }

    public void setLanguage (String language)
    {
        this.language = language;
    }

    public String getForks_count ()
    {
        return forks_count;
    }

    public void setForks_count (String forks_count)
    {
        this.forks_count = forks_count;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getStargazers_count ()
    {
        return stargazers_count;
    }

    public void setStargazers_count (String stargazers_count)
    {
        this.stargazers_count = stargazers_count;
    }

    public String getFull_name ()
    {
        return full_name;
    }

    public void setFull_name (String full_name)
    {
        this.full_name = full_name;
    }

    public String getOwnerLogin(){
        return owner.getLogin();
    }

}
