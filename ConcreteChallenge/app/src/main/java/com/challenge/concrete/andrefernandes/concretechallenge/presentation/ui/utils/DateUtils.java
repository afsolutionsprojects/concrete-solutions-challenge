package com.challenge.concrete.andrefernandes.concretechallenge.presentation.ui.utils;

import org.joda.time.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by andrefernandes on 9/18/16.
 */
public class DateUtils {

    public static String formattedDate(String date){
        DateTime dt = new DateTime(date);

        String day = dt.dayOfMonth().getAsString();
        String month = dt.monthOfYear().getAsString();
        String year = dt.year().getAsString();

        String dateString = day + "/" + month + "/" + year;

        return dateString;
    }
}
