package com.challenge.concrete.andrefernandes.concretechallenge.presentation.presenters.impl;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;

import com.challenge.concrete.andrefernandes.concretechallenge.domain.executor.Executor;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.executor.MainThread;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.interactors.GetAllRepositoriesInteractor;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.interactors.impl.GetAllRepositoriesImpl;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.repositories.RepositoryModel;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.repository.RepositoriesRepository;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.presenters.MainPresenter;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.presenters.base.AbstractPresenter;

import java.util.List;

/**
 * Created by andrefernandes on 9/14/16.
 */
public class MainPresenterImpl extends AbstractPresenter implements MainPresenter, GetAllRepositoriesInteractor.Callback {

    private MainPresenter.View view;
    private RepositoriesRepository repository;
    private int page;

    public MainPresenterImpl(Executor executor, MainThread mainThread, View view, RepositoriesRepository repository) {
        super(executor, mainThread);
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void resume() {
        view.showProgress();
        page = 1;
        GetAllRepositoriesInteractor interactor = new GetAllRepositoriesImpl(executor, mainThread, this, repository, page);
        interactor.execute();
    }

    @Override
    public void pause() {
    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {
        view.showError("");
    }

    @Override
    public void onRepositoriesRetrieved(RepositoryModel repositories) {
        view.showRepositories(repositories);
        view.hideProgress();

    }

    @Override
    public void onRetrieveFailed() {
        if (page == 1) {
            view.showError("");
        } else {
            view.showSnackbarError();
        }

    }

    @Override
    public void loadMoreRepositores(int page) {
        this.page = page;
        GetAllRepositoriesInteractor interactor = new GetAllRepositoriesImpl(executor, mainThread, this, repository, page);
        interactor.execute();
    }
}
