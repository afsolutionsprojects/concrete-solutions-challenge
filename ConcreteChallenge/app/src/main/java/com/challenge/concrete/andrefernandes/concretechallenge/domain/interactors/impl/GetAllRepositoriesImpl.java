package com.challenge.concrete.andrefernandes.concretechallenge.domain.interactors.impl;

import com.challenge.concrete.andrefernandes.concretechallenge.domain.executor.Executor;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.executor.MainThread;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.interactors.GetAllRepositoriesInteractor;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.interactors.base.AbstractInteractor;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.repositories.RepositoryModel;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.repository.RepositoriesRepository;

import java.util.List;

/**
 * Created by andrefernandes on 9/13/16.
 */
public class GetAllRepositoriesImpl extends AbstractInteractor implements GetAllRepositoriesInteractor{

    private Callback callback;
    private RepositoriesRepository repository;
    private int page;


    public GetAllRepositoriesImpl(Executor threadExecutor, MainThread mainThread, Callback callback,
                                  RepositoriesRepository repository, int page) {
        super(threadExecutor, mainThread);
        this.callback = callback;
        this.repository = repository;
        this.page = page;

    }

    private void  notifyError(){
        this.mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onRetrieveFailed();
            }
        });
    }

    private void postRepositories(final RepositoryModel repositoryModelList){
        this.mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onRepositoriesRetrieved(repositoryModelList);
            }
        });
    }

    @Override
    public void run() {
        RepositoryModel repositoryModels = repository.getAllRepositories(page);

        if(repositoryModels != null && repositoryModels.getItems().size() > 0){
            postRepositories(repositoryModels);
        } else {
            notifyError();
        }
    }

    public void loadRepositoriesByPage(int page){
        this.page = page;
        run();
    }
}
