package com.challenge.concrete.andrefernandes.concretechallenge.domain.network;

import com.challenge.concrete.andrefernandes.concretechallenge.BuildConfig;

import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by andrefernandes on 9/13/16.
 */
public class RestAdapterProvider {

    private static final RestAdapterProvider INSTANCE = new RestAdapterProvider();

    private final String BASE_URL = "https://api.github.com";

    Retrofit retrofit;

    private RestAdapterProvider() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient())
                .build();
    }

    public static RestAdapterProvider instance() {
        return INSTANCE;
    }

    public Retrofit getAdapter() {
        return retrofit;
    }


    public OkHttpClient getClient() {

        OkHttpClient client = new OkHttpClient
                .Builder()
                .connectTimeout(90, TimeUnit.SECONDS)
                .writeTimeout(90, TimeUnit.SECONDS)
                .readTimeout(90, TimeUnit.SECONDS)
                .build();

        return client;
    }
}
