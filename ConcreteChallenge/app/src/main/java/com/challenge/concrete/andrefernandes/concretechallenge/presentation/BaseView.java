package com.challenge.concrete.andrefernandes.concretechallenge.presentation;

/**
 * Created by andrefernandes on 9/14/16.
 */
public interface BaseView {

    void showProgress();

    void hideProgress();

    void showError(String message);
}
