package com.challenge.concrete.andrefernandes.concretechallenge.presentation.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.challenge.concrete.andrefernandes.concretechallenge.R;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.pullrequests.PullRequestsModel;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.pullrequests.User;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.ui.Navigator;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.ui.listeners.RecyclerViewClickListener;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.ui.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by andrefernandes on 9/18/16.
 */
public class PullRequestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements RecyclerViewClickListener {

    private Context context;
    private ArrayList<PullRequestsModel> pullRequestsModel;

    public PullRequestAdapter(Context context) {
        this.context = context;
        pullRequestsModel = new ArrayList<PullRequestsModel>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.pullrequest_item, parent, false);
        return new ViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        PullRequestsModel model = pullRequestsModel.get(position);
        ((ViewHolder) holder).setup(model, context);
    }

    @Override
    public int getItemCount() {
        return pullRequestsModel.size();
    }

    public void addNewPullRequest(@NonNull List<PullRequestsModel> pullRequestResponseModel) {

        this.pullRequestsModel.addAll(pullRequestResponseModel);
        notifyDataSetChanged();
    }

    @Override
    public void onClickView(int position) {
        Navigator navigator = new Navigator((Activity)context);
        String url = pullRequestsModel.get(position).getHtml_url();
        navigator.goToPullRequestWeb(url);
    }

    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.pullrequest_title)
        TextView pullrequestTitle;
        @Bind(R.id.pullrequest_description)
        TextView pullrequestDescription;
        @Bind(R.id.pullrequest_user_image)
        ImageView pullrequestUserImage;
        @Bind(R.id.username)
        TextView username;
        @Bind(R.id.pr_created_at_text)
        TextView prCreatedAtText;

        private RecyclerViewClickListener listener;

        public void setup(PullRequestsModel pullRequestsModel, final Context context) {

            User user = pullRequestsModel.getUser();
            pullrequestTitle.setText(pullRequestsModel.getTitle());
            pullrequestDescription.setText(pullRequestsModel.getBody());
            username.setText(user.getLogin());

            String formattedDate = DateUtils.formattedDate(pullRequestsModel.getCreated_at());
            prCreatedAtText.setText(formattedDate);

            Glide.with(context).load(user.getAvatar_url()).asBitmap().centerCrop()
                    .placeholder(context.getDrawable(R.drawable.ic_perm_identity_black_24dp))
                    .into(new BitmapImageViewTarget(pullrequestUserImage) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            pullrequestUserImage.setImageDrawable(circularBitmapDrawable);
                        }
                    });
        }

        public ViewHolder(View view, RecyclerViewClickListener listener) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
            this.listener = listener;
        }

        @Override
        public void onClick(View view) {
            listener.onClickView(getAdapterPosition());
        }
    }
}
