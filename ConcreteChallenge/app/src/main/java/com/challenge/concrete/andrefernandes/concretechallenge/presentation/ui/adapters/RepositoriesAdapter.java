package com.challenge.concrete.andrefernandes.concretechallenge.presentation.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.challenge.concrete.andrefernandes.concretechallenge.R;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.repositories.OwnerRepository;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.repositories.RepositoryItem;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.repositories.RepositoryModel;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.presenters.MainPresenter;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.ui.Navigator;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.ui.activities.MainActivity;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.ui.listeners.RecyclerViewClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by andrefernandes on 9/17/16.
 */
public class RepositoriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements RecyclerViewClickListener {

    private List<RepositoryItem> repositoryItemList;
    private Context context;

    public RepositoriesAdapter(Context context) {
        repositoryItemList = new ArrayList<>();
        this.context = context;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.repository_item, parent, false);
        return new ViewHolder(view, this);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder) holder).setup(repositoryItemList.get(position), context);
    }


    @Override
    public int getItemCount() {
        return repositoryItemList.size();
    }

    @Override
    public void onClickView(int position) {
        Navigator navigator = new Navigator((Activity)context);
        navigator.goToPullRequest(repositoryItemList.get(position));
    }

    public void addNewRepositories(@NonNull RepositoryModel repositoryModel) {
        repositoryItemList.addAll(repositoryModel.getItems());
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @Bind(R.id.repository_name)
        TextView repositoryName;
        @Bind(R.id.description)
        TextView description;
        @Bind(R.id.fork_icon)
        ImageView forkIcon;
        @Bind(R.id.fork_text)
        TextView forkText;
        @Bind(R.id.star_icon)
        ImageView starIcon;
        @Bind(R.id.stars_text)
        TextView starsText;
        @Bind(R.id.username)
        TextView username;
        @Bind(R.id.user_image)
        ImageView userImage;
        @Bind(R.id.name)
        TextView name;

        private RecyclerViewClickListener listener;

        public void setup(RepositoryItem repositoryItem, final Context context) {

            OwnerRepository owner = repositoryItem.getOwner();
            repositoryName.setText(repositoryItem.getFull_name());
            description.setText(repositoryItem.getDescription());
            forkText.setText(repositoryItem.getForks_count());
            starsText.setText(repositoryItem.getStargazers_count());
            username.setText(owner.getLogin());

            Glide.with(context).load(owner.getAvatar_url()).asBitmap().centerCrop()
                    .placeholder(context.getDrawable(R.drawable.ic_perm_identity_black_24dp))
                    .into(new BitmapImageViewTarget(userImage) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    userImage.setImageDrawable(circularBitmapDrawable);
                }
            });
        }

        ViewHolder(View view, final RecyclerViewClickListener listener) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
            this.listener = listener;
        }

        @Override
        public void onClick(View view) {
            listener.onClickView(getAdapterPosition());
        }
    }
}
