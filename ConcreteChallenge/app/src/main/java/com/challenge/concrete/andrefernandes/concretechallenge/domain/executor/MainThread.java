package com.challenge.concrete.andrefernandes.concretechallenge.domain.executor;

/**
 * Created by andrefernandes on 9/13/16.
 */
public interface MainThread {

    void post(final Runnable runnable);
}
