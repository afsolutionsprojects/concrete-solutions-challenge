package com.challenge.concrete.andrefernandes.concretechallenge.domain.network;

import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.pullrequests.PullRequestsModel;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.repositories.RepositoryModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by andrefernandes on 9/13/16.
 */
public interface GithubAPI {
    @GET("search/repositories")
    Call<RepositoryModel> getAllRepositoriesJava(
            @Query("q") String language,
            @Query("sort") String stars,
            @Query("page") int page
    );

    @GET("repos/{login}/{repository_name}/pulls")
    Call<List<PullRequestsModel>> getPullRequestByRepositoryName(@Path("login") String login,
                                                                 @Path("repository_name") String repositoryName);
}
