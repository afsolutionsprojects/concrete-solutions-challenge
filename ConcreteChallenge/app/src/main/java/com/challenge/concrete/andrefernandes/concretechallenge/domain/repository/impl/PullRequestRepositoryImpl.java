package com.challenge.concrete.andrefernandes.concretechallenge.domain.repository.impl;

import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.pullrequests.PullRequestsModel;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.network.GithubAPI;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.network.RestAdapterProvider;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.repository.PullRequestRepository;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * Created by andrefernandes on 9/18/16.
 */
public class PullRequestRepositoryImpl implements PullRequestRepository {

    private GithubAPI api;


    public PullRequestRepositoryImpl() {
        Retrofit adapterProvider = RestAdapterProvider.instance().getAdapter();
        api = adapterProvider.create(GithubAPI.class);
    }


    @Override
    public List<PullRequestsModel> getPullRequest(String login, String repositoryName) {
        Call<List<PullRequestsModel>> call = api.getPullRequestByRepositoryName(login, repositoryName);
        try {
            return call.execute().body();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
