package com.challenge.concrete.andrefernandes.concretechallenge.presentation.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.repositories.RepositoryItem;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.ui.activities.PullRequestActivity;

/**
 * Created by andrefernandes on 9/18/16.
 */
public class Navigator {

    Activity activity;

    public Navigator(Activity activity){
        this.activity = activity;
    }

    public void goToPullRequest(RepositoryItem repositoryItem){
        if (activity != null) {
            Intent intentToLaunch = PullRequestActivity.getCallingIntent(activity, repositoryItem);
            activity.startActivity(intentToLaunch);
        }
    }

    public void goToPullRequestWeb(String url){
        Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        activity.startActivity(intent);
    }
}
