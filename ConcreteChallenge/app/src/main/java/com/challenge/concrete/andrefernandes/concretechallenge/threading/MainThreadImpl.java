package com.challenge.concrete.andrefernandes.concretechallenge.threading;

import android.os.Handler;
import android.os.Looper;

import com.challenge.concrete.andrefernandes.concretechallenge.domain.executor.MainThread;

/**
 * Created by andrefernandes on 9/13/16.
 */
public class MainThreadImpl implements MainThread {

    private static MainThread sMainThread;

    private Handler mHandler;

    public MainThreadImpl() {
        mHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void post(Runnable runnable) {
        mHandler.post(runnable);
    }

    public static MainThread getInstance() {
        if (sMainThread == null) {
            sMainThread = new MainThreadImpl();
        }

        return sMainThread;
    }
}
