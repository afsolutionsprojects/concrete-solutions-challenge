package com.challenge.concrete.andrefernandes.concretechallenge.presentation.presenters;

import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.pullrequests.PullRequestsModel;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.BaseView;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.presenters.base.BasePresenter;

import java.util.List;

/**
 * Created by andrefernandes on 9/18/16.
 */
public interface PullRequestPresenter extends BasePresenter{

    interface View extends BaseView {
        void showPullRequests(List<PullRequestsModel> pullRequestsModel);
        void insertOpenedAndClosed(String opened, String closed);
    }
}
