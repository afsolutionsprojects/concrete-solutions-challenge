package com.challenge.concrete.andrefernandes.concretechallenge.domain.repository.impl;

import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.repositories.RepositoryModel;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.network.GithubAPI;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.network.RestAdapterProvider;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.repository.RepositoriesRepository;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * Created by andrefernandes on 9/13/16.
 */
public class RepositoriesRepositoryImpl implements RepositoriesRepository {

    private GithubAPI api;
    private final String LANGUAGE = "Java";
    private final String ORDER = "stars";


    public RepositoriesRepositoryImpl() {
        Retrofit adapterProvider = RestAdapterProvider.instance().getAdapter();
        api = adapterProvider.create(GithubAPI.class);
    }

    @Override
    public RepositoryModel getAllRepositories(int page) {
        String query = buildLanguageQuery();
        Call<RepositoryModel> call = api.getAllRepositoriesJava(query, ORDER, page);
        try {
            return call.execute().body();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private String buildLanguageQuery() {
        return "language:" + LANGUAGE;
    }
}
