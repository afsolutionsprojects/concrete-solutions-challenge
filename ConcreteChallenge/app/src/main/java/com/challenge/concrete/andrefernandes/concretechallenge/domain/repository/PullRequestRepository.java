package com.challenge.concrete.andrefernandes.concretechallenge.domain.repository;

import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.pullrequests.PullRequestsModel;

import java.util.List;

/**
 * Created by andrefernandes on 9/18/16.
 */
public interface PullRequestRepository {

    List<PullRequestsModel> getPullRequest(String login, String repositoryName);
}
