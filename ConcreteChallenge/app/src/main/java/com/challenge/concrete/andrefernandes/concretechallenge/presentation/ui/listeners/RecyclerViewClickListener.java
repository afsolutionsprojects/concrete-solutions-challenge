package com.challenge.concrete.andrefernandes.concretechallenge.presentation.ui.listeners;

/**
 * Created by andrefernandes on 9/17/16.
 */
public interface RecyclerViewClickListener {

    void onClickView(int position);
}
