package com.challenge.concrete.andrefernandes.concretechallenge.domain.executor;

import com.challenge.concrete.andrefernandes.concretechallenge.domain.interactors.base.AbstractInteractor;

/**
 * Created by andrefernandes on 9/13/16.
 */
public interface Executor {

    void execute(final AbstractInteractor interactor);

}
