package com.challenge.concrete.andrefernandes.concretechallenge.presentation.presenters;

import android.support.v7.widget.RecyclerView;

import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.repositories.RepositoryModel;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.BaseView;
import com.challenge.concrete.andrefernandes.concretechallenge.presentation.presenters.base.BasePresenter;

import java.util.List;

/**
 * Created by andrefernandes on 9/14/16.
 */
public interface MainPresenter extends BasePresenter {

    void loadMoreRepositores(int page);

    interface View extends BaseView {
        void showRepositories(RepositoryModel repositoryModelList);
        void showSnackbarError();
    }

}
