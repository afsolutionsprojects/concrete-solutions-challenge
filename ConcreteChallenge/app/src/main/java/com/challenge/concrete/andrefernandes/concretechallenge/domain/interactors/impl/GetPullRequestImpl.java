package com.challenge.concrete.andrefernandes.concretechallenge.domain.interactors.impl;

import com.challenge.concrete.andrefernandes.concretechallenge.domain.executor.Executor;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.executor.MainThread;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.interactors.GetPullRequestInteractor;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.interactors.base.AbstractInteractor;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.model.pullrequests.PullRequestsModel;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.repository.PullRequestRepository;

import java.util.List;

/**
 * Created by andrefernandes on 9/18/16.
 */
public class GetPullRequestImpl extends AbstractInteractor implements GetPullRequestInteractor {

    private Callback callback;
    private PullRequestRepository repository;
    private String login;
    private String repositoryName;

    public GetPullRequestImpl(Executor threadExecutor, MainThread mainThread,
                              GetPullRequestInteractor.Callback callback, PullRequestRepository repository,
                              String login, String repositoryName) {
        super(threadExecutor, mainThread);
        this.callback = callback;
        this.repository = repository;
        this.login = login;
        this.repositoryName = repositoryName;
    }

    private void postPullRequest(final List<PullRequestsModel> pullRequestsModel){
        this.mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onPullRequestRetrieved(pullRequestsModel);
            }
        });
    }

    private void notifyError(){
        this.mainThread.post(new Runnable() {
            @Override
            public void run() {
                callback.onRetrieveFailed();
            }
        });
    }

    @Override
    public void run() {
        List<PullRequestsModel> pullRequest = repository.getPullRequest(login, repositoryName);

        if(pullRequest != null){
            postPullRequest(pullRequest);
        } else {
            notifyError();
        }
    }
}
