package com.challenge.concrete.andrefernandes.concretechallenge.presentation.presenters.base;

/**
 * Created by andrefernandes on 9/14/16.
 */
public interface BasePresenter {

    void resume();

    void pause();

    void stop();

    void destroy();

    void onError(String message);
}
