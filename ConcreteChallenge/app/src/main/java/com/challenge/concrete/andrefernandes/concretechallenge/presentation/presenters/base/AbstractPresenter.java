package com.challenge.concrete.andrefernandes.concretechallenge.presentation.presenters.base;

import com.challenge.concrete.andrefernandes.concretechallenge.domain.executor.Executor;
import com.challenge.concrete.andrefernandes.concretechallenge.domain.executor.MainThread;

/**
 * Created by andrefernandes on 9/14/16.
 */
public abstract class AbstractPresenter {

    protected Executor  executor;
    protected MainThread mainThread;

    public AbstractPresenter(Executor executor, MainThread mainThread) {
        this.executor = executor;
        this.mainThread = mainThread;
    }

}
